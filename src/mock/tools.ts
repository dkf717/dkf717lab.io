import { failResponseWrap, successResponseWrap } from '@/utils/setup-mock';
import { DYDB_FIELD, DYDB_PAGE } from '@/utils/constant';
import DbTable from './classUtils';
// 数据库对象
const DbO: any = {};
// 字段列表数据库初始化配置
const dynamicFieldConfig = {
  id: 'text primary key',
  dynamicDbId: 'text', // 数据库id--关联
  field: 'text', // 字段名
  title: 'text', // 字段标题
  dbSet: `text default 'text'`, // 数据库设置
};
// 数据库配置对象
const DbConfigO: any = {
  // 动态页面通用配置
  dynamicConfigBase: {
    id: 'text primary key',
    dynamicPageId: 'text', // 页面id--关联
    pageType: `text default 'table'`, // 页面类型
    treeDataApi: 'text', // 树获取api
    treeAddApi: 'text', // 树新增api
    treeUpdateApi: 'text', // 树更新api
    treeDelApi: 'text', // 树删除api
    tableDataApi: 'text', // 列表获取api
    tableAddApi: 'text', // 列表新增api
    tableUpdateApi: 'text', // 列表更新api
    tableDelApi: 'text', // 列表删除api
    treePageId: 'text', // 树页面id
    treePageName: 'text', // 树页面名称
    tableCorrelateField: 'text', // 列表关联字段
  },
  dynamicTableBase: {
    id: 'text primary key',
    dynamicPageId: 'text', // 页面id--关联
    addBtnShow: 'boolean default true', // 新增按钮展示
    addBtnIcon: 'text', // 新增按钮图标
    addBtnText: 'text', // 新增按钮文字
    canView: 'boolean default true', // 能否查看
    viewBtnIcon: 'text', // 查看按钮图标
    viewBtnText: 'text', // 查看按钮文字
    delBtnShow: 'boolean default true', // 新增按钮展示
    delBtnIcon: 'text', // 新增按钮图标
    delBtnText: 'text', // 新增按钮文字
    getDataListApi: 'text', // 获取列表api
    addNewApi: 'text', // 新增数据api
  },
  dynamicTableColumn: {
    id: 'text primary key',
    dynamicPageId: 'text', // 页面id--关联
    field: 'text', // 字段名
    title: 'text', // 字段标题
    align: `text default 'left'`, // 对齐方式
    width: 'int default 0', // 宽度
    tableShow: 'boolean default false', // 列表中展示
    searchShow: 'boolean default false', // 搜索中展示
    canSet: 'boolean default false', // 能否配置
    isCustom: 'boolean default false', // 是否自定义
    isDel: 'boolean default false', // 是否删除
    orderNo: 'int default 5555', // 排序
  },
  // 调查问卷题目
  questionnaireQuestion: {
    id: 'text primary key',
    questionnaireId: 'text', // 问卷id
    type: 'text', // 类型
    title: 'text', // 标题
    required: 'boolean default true', // 必填
    orderNo: 'int default 5555', // 排序
    options: 'text', // 选项JSON
    isShow: 'boolean default true', // 是否展示
    selectNum: 'text', // 选择数量JSON
  },
  // 调查问卷题目显示条件
  questionnaireQuestionDisplay: {
    id: 'text primary key',
    questionId: 'text', // 题目id
    condition: 'text', // 条件JSON
  },
};
// 直接获取字段列表数据库由DbConfigO.init生成
export const getDynamicFieldDb = async () => {
  // 若是已生成过一次直接返回
  if (DbO.dynamicField) return DbO.dynamicField;
  const dynamicFieldDb = new DbTable();
  // 先进行检测
  const dynamicFieldDbExist = await dynamicFieldDb.checkExist(
    `dynamic${DYDB_FIELD}`
  );
  // 在检测结果判断前先进行创建以免循环进入该方法
  await dynamicFieldDb.createTable(`dynamic${DYDB_FIELD}`, dynamicFieldConfig);
  DbO.dynamicField = dynamicFieldDb;
  // 若没有字段列表数据库存在则执行数据库初始化方法
  if (!dynamicFieldDbExist) {
    // eslint-disable-next-line import/no-cycle
    const { initDb } = await import('./initDb');
    await initDb();
  }
  return dynamicFieldDb;
};
// 获取数据库操作对象
export const getDb = async (dbName: string) => {
  // 若是已生成过一次的数据库直接返回
  if (DbO[dbName]) return DbO[dbName];
  // 初始化数据库对象，暂未指定数据库名称
  const db = new DbTable();
  // 如果是固定数据库使用DbConfigO配置生成
  if (DbConfigO[dbName]) {
    await db.createTable(dbName, DbConfigO[dbName]);
    DbO[dbName] = db;
    return db;
  }
  // 获取字段列表
  const dynamicFieldDb = await getDynamicFieldDb();
  const { list: fieldList }: { list: any[] } = await dynamicFieldDb.getDataList(
    {
      search: { dynamicDbId: dbName },
    }
  );
  const config: any = {};
  fieldList.forEach((v) => {
    config[v.field] = v.dbSet;
  });
  await db.createTable(`dynamic${dbName}`, config);
  DbO[dbName] = db;
  return db;
};

// 获取动态数据库操作对象
export const getDynamicDb = async (pageId: string) => {
  if (DbO[pageId]) return DbO[pageId];
  const dynamicPageDb = await getDb(DYDB_PAGE);
  const {
    list: [{ dynamicDbId }],
  }: any = await dynamicPageDb.getDataList({ search: { id: pageId } });
  const db = await getDb(dynamicDbId);
  DbO[pageId] = db;
  return db;
};
// 初始化接口拦截
export const initMockData = (dbName: string) => ({
  // 获取列表
  [`/api/${dbName}/dataList`]: async (data: any) => {
    try {
      const db = await getDb(dbName);
      const res = await db.getDataList(data);
      return successResponseWrap(res);
    } catch (err: any) {
      return failResponseWrap(err.message);
    }
  },
  // 新增并返回新增后的数据
  [`/api/${dbName}/addNew`]: async (data: any) => {
    try {
      const db = await getDb(dbName);
      await db.addData(data);
      const res = await db.getDataList({
        search: { id: [].concat(data).map((v: any) => v.id) },
      });
      return successResponseWrap(res);
    } catch (err: any) {
      return failResponseWrap(err.message);
    }
  },
  // 通过Id删除
  [`/api/${dbName}/delById`]: async (id: any) => {
    try {
      const db = await getDb(dbName);
      await db.deleteData({ search: { id } });
      return successResponseWrap({});
    } catch (err: any) {
      return failResponseWrap(err.message);
    }
  },
  // 更新并返回更新后数据
  [`/api/${dbName}/update`]: async (data: any) => {
    try {
      const db = await getDb(dbName);
      await db.updateData(data);
      const res = await db.getDataList(data);
      return successResponseWrap(res);
    } catch (err: any) {
      return failResponseWrap(err.message);
    }
  },
});
