import { failResponseWrap, successResponseWrap } from '@/utils/setup-mock';
import { getDb } from '../tools';

export default {
  '/api/questionnaire/getData': async (questionnaireId: string) => {
    try {
      // 获取题目
      const questionnaireQuestionDb = await getDb('questionnaireQuestion');
      const { list: questions } = await questionnaireQuestionDb.getDataList({
        search: { questionnaireId },
        orderBy: 'orderNo',
      });
      questions.forEach((v: any) => {
        v.required = !!v.required;
        v.isShow = !!v.isShow;
        if (v.options) {
          try {
            v.options = JSON.parse(v.options);
          } catch (err) {
            v.options = [];
          }
        }
        if (v.selectNum) {
          try {
            v.selectNum = JSON.parse(v.selectNum);
          } catch (err) {
            v.selectNum = [1, 1];
          }
        }
      });
      // 获取关联关系

      const questionnaireQuestionDisplayDb = await getDb(
        'questionnaireQuestionDisplay'
      );
      const { list: displayCondition } =
        await questionnaireQuestionDisplayDb.getDataList({
          search: { questionId: questions.map((v: any) => v.id) },
        });
      displayCondition.forEach((v: any) => {
        if (v.condition) {
          try {
            v.condition = JSON.parse(v.condition);
          } catch (err) {
            v.condition = [];
          }
        }
      });
      // baseConfig.addBtnShow = !!baseConfig.addBtnShow;
      // baseConfig.canView = !!baseConfig.canView;
      // baseConfig.delBtnShow = !!baseConfig.delBtnShow;

      // // 获取表列设置
      // const dynamicTableColumnDb = await getDb('dynamicTableColumn');
      // const { list: tableColumnList } = await dynamicTableColumnDb.getDataList({
      //   search: { dynamicPageId: pageId, /* canSet: true, */ isDel: false },
      //   orderBy: 'orderNo',
      // });
      // tableColumnList.forEach((v: any) => {
      //   v.tableShow = !!v.tableShow;
      //   v.searchShow = !!v.searchShow;
      //   v.isCustom = !!v.isCustom;
      // });
      return successResponseWrap({
        questions,
        displayCondition,
      });
    } catch (err: any) {
      return failResponseWrap(err.message);
    }
  },
  '/api/questionnaire/saveData': async (data: any) => {
    try {
      console.log(5588, data);
      const { questions } = JSON.parse(JSON.stringify(data));
      questions.forEach((v: any) => {
        v.options = JSON.stringify(v.options);
        v.selectNum = JSON.stringify(v.selectNum);
      });
      const questionnaireQuestionDb = await getDb('questionnaireQuestion');
      await questionnaireQuestionDb.addData(questions);
      // 获取题目
      //   const questionnaireQuestionDb = await getDb('questionnaireQuestion');
      //   const { list: questions } = await questionnaireQuestionDb.getDataList({
      //     search: { questionnaireId },
      //     orderBy: 'orderNo',
      //   });
      //   questions.forEach((v: any) => {
      //     v.required = !!v.required;
      //     v.isShow = !!v.isShow;
      //     if (v.options) {
      //       try {
      //         v.options = JSON.parse(v.options);
      //       } catch (err) {
      //         v.options = [];
      //       }
      //     }
      //     if (v.selectNum) {
      //       try {
      //         v.selectNum = JSON.parse(v.selectNum);
      //       } catch (err) {
      //         v.selectNum = [1, 1];
      //       }
      //     }
      //   });
      //   // 获取关联关系

      //   const questionnaireQuestionDisplayDb = await getDb(
      //     'questionnaireQuestionDisplay'
      //   );
      //   const { list: displayCondition } =
      //     await questionnaireQuestionDisplayDb.getDataList({
      //       search: { questionId: questions.map((v: any) => v.id) },
      //     });
      //   displayCondition.forEach((v: any) => {
      //     if (v.condition) {
      //       try {
      //         v.condition = JSON.parse(v.condition);
      //       } catch (err) {
      //         v.condition = [];
      //       }
      //     }
      //   });
      //   // baseConfig.addBtnShow = !!baseConfig.addBtnShow;
      //   // baseConfig.canView = !!baseConfig.canView;
      //   // baseConfig.delBtnShow = !!baseConfig.delBtnShow;

      //   // // 获取表列设置
      //   // const dynamicTableColumnDb = await getDb('dynamicTableColumn');
      //   // const { list: tableColumnList } = await dynamicTableColumnDb.getDataList({
      //   //   search: { dynamicPageId: pageId, /* canSet: true, */ isDel: false },
      //   //   orderBy: 'orderNo',
      //   // });
      //   // tableColumnList.forEach((v: any) => {
      //   //   v.tableShow = !!v.tableShow;
      //   //   v.searchShow = !!v.searchShow;
      //   //   v.isCustom = !!v.isCustom;
      //   // });
      return successResponseWrap({
        // questions,
        // displayCondition,
      });
    } catch (err: any) {
      return failResponseWrap(err.message);
    }
  },
};
