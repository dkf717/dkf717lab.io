import { getRandomId } from '@/utils/publicJs';
import { DYDB_DB, DYDB_FIELD, DYDB_PAGE, DYDB_ROUTER } from '@/utils/constant';
import dynamic from './dynamic';
// eslint-disable-next-line import/no-cycle
import { getDb, getDynamicFieldDb } from './tools';

const mockData: any = { ...dynamic };
// eslint-disable-next-line import/prefer-default-export
export const initDb = async () => {
  try {
    const dynamicFieldDb = await getDynamicFieldDb();
    const fieldCongig: any = {
      [DYDB_DB]: {
        id: 'text primary key',
        name: 'text',
      },
      [DYDB_FIELD]: {
        id: 'text primary key',
        dynamicDbId: 'text',
        field: 'text',
        title: 'text',
        dbSet: `text default 'text'`,
      },
      [DYDB_PAGE]: {
        id: 'text primary key',
        dynamicDbId: 'text',
        name: 'text',
      },
      [DYDB_ROUTER]: {
        id: 'text primary key',
        parentName: 'text',
        path: 'text',
        name: 'text',
        component: 'text',
        title: 'text',
        icon: 'text',
        params: 'text',
        orderNo: 'int default 5555',
      },
    };
    const fieldAddData = [].concat(
      ...Object.keys(fieldCongig).map((dynamicDbId) =>
        Object.keys(fieldCongig[dynamicDbId]).map(
          (field) =>
            ({
              id: getRandomId(),
              dynamicDbId,
              field,
              dbSet: fieldCongig[dynamicDbId][field],
            } as never)
        )
      )
    );
    await dynamicFieldDb.addData(fieldAddData);
    const dbAddData = [
      {
        id: DYDB_FIELD,
        name: '库字段',
      },
      {
        id: DYDB_DB,
        name: '库结构',
      },
      {
        id: DYDB_PAGE,
        name: '页面',
      },
      {
        id: DYDB_ROUTER,
        name: '路由',
      },
    ];
    const dynamicdbDb = await getDb(DYDB_DB);
    await dynamicdbDb.addData(dbAddData);
    // 初始化dynamicpageDb数据
    const fieldPageId = getRandomId();
    const dbPageId = getRandomId();
    const pagePageId = getRandomId();
    const routerPageId = getRandomId();
    const routerAddData = [
      {
        id: getRandomId(),
        parentName: '',
        path: '/temporary',
        name: 'temporary',
        component: 'layout',
        title: '临时目录',
        icon: 'icon-close',
        orderNo: 1000,
        params: '',
      },
      {
        id: getRandomId(),
        parentName: 'temporary',
        path: '/dynamic/:pageId',
        name: 'dbField',
        component: '@/views/dynamicPage/index.vue',
        title: '字段列表',
        icon: '',
        orderNo: 1000,
        params: `{"pageId":"${fieldPageId}"}`,
      },
      {
        id: getRandomId(),
        parentName: 'temporary',
        path: '/dynamic/:pageId',
        name: 'db',
        component: '@/views/dynamicPage/index.vue',
        title: '库列表',
        icon: '',
        orderNo: 1000,
        params: `{"pageId":"${dbPageId}"}`,
      },
      {
        id: getRandomId(),
        parentName: 'temporary',
        path: '/dynamic/:pageId',
        name: 'page',
        component: '@/views/dynamicPage/index.vue',
        title: '页面列表',
        icon: '',
        orderNo: 1000,
        params: `{"pageId":"${pagePageId}"}`,
      },
      {
        id: getRandomId(),
        parentName: 'temporary',
        path: '/dynamic/:pageId',
        name: 'router',
        component: '@/views/dynamicPage/index.vue',
        title: '路由列表',
        icon: '',
        orderNo: 1000,
        params: `{"pageId":"${routerPageId}"}`,
      },
    ];
    const dynamicrouterDb = await getDb(DYDB_ROUTER);
    await dynamicrouterDb.addData(routerAddData);
    // 生成页面配置
    await mockData[`/api/page/addNew`]({
      id: fieldPageId,
      dynamicDbId: DYDB_FIELD,
      name: '字段列表',
    });
    await mockData[`/api/page/addNew`]({
      id: dbPageId,
      dynamicDbId: DYDB_DB,
      name: '库列表',
    });
    await mockData[`/api/page/addNew`]({
      id: pagePageId,
      dynamicDbId: DYDB_PAGE,
      name: '页面列表',
    });
    await mockData[`/api/page/addNew`]({
      id: routerPageId,
      dynamicDbId: DYDB_ROUTER,
      name: '路由列表',
    });
    // 修改页面通用配置
    const dynamicConfigBaseDb = await getDb('dynamicConfigBase');
    await dynamicConfigBaseDb.updateData({
      data: { pageType: 'treeTable' },
      search: { dynamicPageId: fieldPageId },
    });
    const dynamicTableBaseDb = await getDb('dynamicTableBase');
    await dynamicTableBaseDb.updateData({
      data: { addNewApi: '/api/page/addNew' },
      search: { dynamicPageId: pagePageId },
    });
  } catch (err) {
    /* empty */
  }
};
