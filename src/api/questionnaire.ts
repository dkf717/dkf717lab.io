import http from './http';
// 获取路由列表
export function getQuestionnaire(questionnaireId: string) {
  return http.post<any>('/api/questionnaire/getData', questionnaireId);
}
// 获取路由树
export function saveQuestionnaire(data?: any) {
  return http.post<any>('/api/questionnaire/saveData', data);
}
