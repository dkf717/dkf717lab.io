import http from './http';
// 获取路由列表
export function getRouterDataList(data?: any) {
  return http.post<any>('/api/router/dataList', data);
}
// 获取路由树
export function getRouterDataTree(data?: any) {
  return http.post<any>('/api/router/dataTree', data);
}
// 获取树页面列表
export function getTreePageList(data?: any) {
  return http.post<any>('/api/tree/pageList', data);
}
