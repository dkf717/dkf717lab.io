import http from './http';

export function getConfigurationById(pageId: string) {
  return http.post<any>('/api/configuration/getDataById', pageId);
}

export function saveConfigurationData(data: {
  configBase: any;
  baseConfig: any;
  tableColumnList: any[];
}) {
  return http.post<any>('/api/configuration/saveData', data);
}
// 新增表列
// export function addColumn(data:any) {
//   return axios.post<any>('/api/tableColumn/addColumn', data);
// }
// export function delTem(id: any) {
//   return axios.post<PolicyListRes>('/api/list/tem/del', id);
// }
