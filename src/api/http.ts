import axios from 'axios';
import dynamic from '@/mock/dynamic/index';

let http;
const flag = true;
if (flag) {
  http = {
    post: (url: string, data?: any) => dynamic[url](data),
  };
}
export default http || axios;
