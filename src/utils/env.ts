const debug = import.meta.env.MODE !== 'production' || true;
export const isDev = import.meta.env.MODE === 'development';
export default debug;
