/**
 * 封装操作localstorage本地存储的方法
 */
// eslint-disable-next-line import/prefer-default-export
export const storage = {
  // 取出数据
  get<T>(key: string) {
    const value = localStorage.getItem(key);
    if (value && value !== 'undefined' && value !== 'null') {
      return <T>JSON.parse(value);
    }
    return undefined;
  },
  // 存储
  set(key: string, value: any) {
    localStorage.setItem(key, JSON.stringify(value));
  },
  // 删除数据
  remove(key: string) {
    localStorage.removeItem(key);
  },
};
