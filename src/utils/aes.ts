/**
 * 使用crypto-js 来实现对密码的加密和解密。
 */
import CryptoJs from 'crypto-js';

// eslint-disable-next-line import/prefer-default-export
export const aes = {
  // JS加密
  encrypt(clientId: string) {
    const CryptoJS = CryptoJs;
    const data = `${clientId}--${this.convertDate(new Date())}`;
    const key = CryptoJS.enc.Utf8.parse('607490BE-18CA-43D7-B11A-57E2621B');
    const iv = CryptoJS.enc.Utf8.parse('2D59831C-78AC-4227-B3F3-CE656636');
    const encrypted = CryptoJS.AES.encrypt(data, key, {
      iv,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7,
    });
    return encrypted.ciphertext.toString(CryptoJS.enc.Base64);
  },

  convertDate(date: any) {
    const res = `${date.getFullYear()},${
      date.getMonth() + 1
    },${date.getDate()},${date.getHours()},${date.getMinutes()},${date.getSeconds()}`;
    return res;
  },
};
