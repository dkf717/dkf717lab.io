import resize from './resize'; // 引入指令

const directives: any = {
  // 指令对象
  resize,
};

export default {
  install(app: any) {
    Object.keys(directives).forEach((key) => {
      app.directive(key, directives[key]);
    });
  },
};
