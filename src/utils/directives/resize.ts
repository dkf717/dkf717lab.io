// export const resize: any = {
//   resizeListener(e: any) {
//     console.log(999);
//     if (this.bindVal) {
//       this.bindVal(e);
//     }
//   },
//   bind(el: HTMLElement, binding: any) {
//     this.bindVal = binding.value;
//     el.addEventListener('resize', this.resizeListener);
//   },
//   unbind(el: HTMLElement) {
//     el.removeEventListener('resize', this.resizeListener);
//   },
// };
export default {
  mounted(el: any, binding: any) {
    el.resizeListener = (e: any) => {
      console.log(556, '监听缩放函数执行');
      binding.value(e);
    };
    el.addEventListener('resize', el.resizeListener);
  },

  unmounted(el: any) {
    debugger
    el.removeEventListener('resize', el.resizeListener);
  },
};
