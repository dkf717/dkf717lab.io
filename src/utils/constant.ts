import { isDev } from './env';
// 数据库名
export const DYDB_DB = isDev ? 'devDb' : '187ea6c16da4d2f8b7559'; // 默认库列表;以publicJS中getRandomId方法生成
export const DYDB_FIELD = isDev ? 'devField' : '187ea740f0bf2e01e5487'; // 默认字段列表;以publicJS中getRandomId方法生成
export const DYDB_PAGE = isDev ? 'devPage' : '187ea74e39227d8661a9e'; // 默认页面列表;以publicJS中getRandomId方法生成
export const DYDB_ROUTER = isDev ? 'devRouter' : '187ea751a682001911b6b'; // 默认路由列表;以publicJS中getRandomId方法生成
// 人员信息
export const ACCESS_TOKEN = 'access_token';
export const REFRESH_TOKEN = 'refresh_token';
export const ACCOUNTID = 'accountid';
