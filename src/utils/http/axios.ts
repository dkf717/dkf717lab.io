import axios, { AxiosResponse, AxiosRequestConfig, AxiosError } from 'axios';
// import { auth } from '@/utils';
import baseInfo from './baseUrl';
import { aes } from '../aes';
import router from '../../router/index';

axios.defaults.timeout = 1000 * 60;
axios.defaults.headers.post['Content-Type'] = 'application/json';

// 创建axios实例
const service = axios.create({
  // 根据不同env设置不同的baseURL
  baseURL: import.meta.env.VITE_APP_API_BASE_URL,
});

// axios实例拦截请求
service.interceptors.request.use(
  (config: any) => {
    config.headers = {
      ...config.headers,
      // ...auth.headers(),
      // 你的自定义headers，如token等
    };
    if (localStorage.access_token) {
      config.headers.access_token = localStorage.access_token;
    }
    if (localStorage.accountid) {
      config.headers.accountid = localStorage.accountid;
    }
    // 代理请求拦截
    if (config.isagent) {
      config.headers.client_id = aes.encrypt(baseInfo.clientId);
    } else {
      config.headers.client_id = baseInfo.clientId;
    }
    return config;
  },
  (error: AxiosError) => {
    return Promise.reject(error);
  }
);

// axios实例拦截响应
service.interceptors.response.use(
  // 2xx时触发
  (response: any) => {
    // response.data就是后端返回的数据，结构根据约定来定义
    const { status, statusText } = response;
    // console.log('response.data就是后端返回的数据，结构根据约定来定义',status,response);
    let errMessage = '';
    switch (status) {
      case 500: // 无权限
        errMessage = statusText;
        // eslint-disable-next-line no-console
        if (errMessage) console.error(errMessage);
        break;
      case 401: // token过期
        errMessage = 'Token expired';
        router.push('/login');
        break;
      case 403: // 无权限
        errMessage = 'No permission';
        break;
      default:
        errMessage = statusText;
        // if (errMessage)  showFailToast(errMessage);
        break;
    }

    return response;
  },
  // 非2xx时触发
  (error: AxiosError) => {
    // showFailToast('Network Error...');
    return Promise.reject(error);
  }
);

export type { AxiosResponse, AxiosRequestConfig };

export default service;
