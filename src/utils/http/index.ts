/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */
import service, { AxiosRequestConfig } from './axios';
import { storage } from '../storage';
import { ACCESS_TOKEN } from '../constant';
import router from '../../router';

export * from './types';

export const request = <T = any>(config: AxiosRequestConfig): Promise<T> => {
  return new Promise((resolve, reject) => {
    service
      .request(config)
      .then((res) => {
        // 一些业务处理
        resolve(res.data);
      })
      .catch((err) => {
        // closeToast();
        console.log('request fail:', err);
        const Errcode = err.response.status;
        switch (Errcode) {
          case 401:
            storage.remove(ACCESS_TOKEN);
            router.push('/login');
            return;
          default:
        }
        const { message } = err.response.data.error;
        switch (Errcode) {
          case 500:
            // showToast(message);
            break;
          case 400:
            // showToast(message);
            break;
          default:
        }
      });
  });
};

// 代理
export const agentRequest = <T = any>(config: any): Promise<T> => {
  return new Promise((resolve, reject) => {
    service
      .request(config)
      .then((res) => {
        // 一些业务处理
        resolve(res.data);
      })
      .catch((err) => {
        // closeToast();
        console.log('request fail:', err);
        const Errcode = err.response.status;
        const message = err.response.data.Errmsg;
        switch (Errcode) {
          case 401:
            // showToast(message);
            storage.remove(ACCESS_TOKEN);
            router.push('/login');
            return;
          default:
        }
        switch (Errcode) {
          case 500:
            // showToast(message);
            break;
          case 400:
            // showToast(message);
            break;
          default:
        }
      });
  });
};

const http = {
  get<T = any>(
    url: string,
    params = {},
    config?: AxiosRequestConfig
  ): Promise<T> {
    return request({ url, params, ...config, method: 'GET' });
  },
  post<T = any>(
    url: string,
    data = {},
    config?: AxiosRequestConfig
  ): Promise<T> {
    return request({ url, data, ...config, method: 'POST' });
  },
  agentPost<T = any>(
    url: string,
    data = {},
    config?: AxiosRequestConfig
  ): Promise<T> {
    return agentRequest({
      url,
      data,
      ...config,
      method: 'POST',
      isagent: true,
    });
  },
  put<T = any>(
    url: string,
    data = {},
    config?: AxiosRequestConfig
  ): Promise<T> {
    return request({ url, data, ...config, method: 'PUT' });
  },
  delete<T = any>(
    url: string,
    data = {},
    config?: AxiosRequestConfig
  ): Promise<T> {
    return request({ url, data, ...config, method: 'DELETE' });
  },
  // 上传文件，指定 'Content-Type': 'multipart/form-data'
  upload<T = any>(
    url: string,
    data = {},
    config?: AxiosRequestConfig
  ): Promise<T> {
    return request({
      url,
      data,
      ...config,
      method: 'POST',
      headers: { 'Content-Type': 'multipart/form-data' },
    });
  },
};

export default http;
