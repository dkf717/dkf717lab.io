// 测试api
// const baseInfo = {
//   api:'https://webapi.test.goworkla.cn',
//   apiv2:'https://webapiv2.test.goworkla.cn',
//   agentApi:"https://agentnew.test.goworkla.cn", //代理地址
//   bestApi:"https://webapi.goworkla.cn",
//   clientId:'59c0e143555818d2af8e4a81',     // clientId  （客户端ID）
// }

// 预发布api
const baseInfo = {
  api: 'https://apinet6.goworkla.cn',
  apiv2: 'https://webapiv2.goworkla.cn',
  agentApi: 'https://agentnew.goworkla.cn', // 代理地址
  bestApi: 'https://webapi.goworkla.cn',
  clientId: '59c0e143555818d2af8e4a81', // clientId  （客户端ID）
};

// 正式api
// const baseInfo = {
//     api:'https://webapinet6.goworkla.cn',
//     apiv2:'https://webapiv2.goworkla.cn',
//     agentApi:"https://agentnew.goworkla.cn", //代理地址
//     bestApi:"https://webapi.goworkla.cn",
//     clientId:'59c0e143555818d2af8e4a81',     // clientId  （客户端ID）
// }

export default baseInfo;
